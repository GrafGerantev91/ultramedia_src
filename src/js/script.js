$(window).on('load', function () {
    $('body').addClass('loaded_hiding');
    window.setTimeout(function () {
        $('body').addClass('loaded');
        $('body').removeClass('loaded_hiding');
    }, 500);
});
$(document).ready(function () {
    //Smooth scroll and pageup
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.header').addClass('header--fixed');
            $('.header--fixed').fadeIn('slow');
        }
        else {
            $('.header').removeClass('header--fixed');
            /*$('.header--fixed').fadeOut();*/
        }
    });

    $("a[href^='#']").click(function () {
       const _href = $(this).attr("href");
       $("html, body").animate({scrollTop: $(_href).offset().top+"px"});
       $('nav').removeClass('nav-active');
        $('.burger').removeClass("burger-active");
       return false;
    });

    function SlideSize() {
        let $slider = $('.advantages__content');
        if ($(window).width() <= 574) {
            $slider.slick({
                autoplay: false,
                infinite: true,
                speed: 1500,
                adaptiveHeight: true,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                cssEase: 'linear',
                arrows: false
            });
        } else if (($(window).width() >= 575)) {
            if ($slider.hasClass('slick-initialized')) {
                $slider.slick('unslick');
            }

        }
    }

    SlideSize();
    $(window).resize(SlideSize);
    //Animated video
    $('.advantages-item').hover(function() {
        $(this).find('.item-video').addClass('animated fadeInUp item-video--show'); // Наводим на элемент и  конкретный item-video в конкретно наведенном advantages-item
    }, function() {
        $(this).find('.item-video').removeClass('animated fadeInUp item-video--show');
    });

    //Modal
    $('.btn--advantages').on('click', function () {
        $('.overlay, #order').fadeIn('slow');
        $('body').addClass('no-scroll');
    });
    $('.modal__close').on('click', function () {
        $('.overlay,  #order').fadeOut('slow');
        $('body').removeClass('no-scroll');
    });

    /*Валидация форм*/
    function valideForms(form) {
        $(form).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                /*name: "required",*/
                phone: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                /*name: "Пожалуйста введите свое имя",*/
                name: {
                    required: "Пожалуйста введите свое имя",
                    minlength: jQuery.validator.format("Введите не менее 4 символов!")
                },
                phone: "Пожалуйста введите свой телефон",
                email: {
                    required: "Введите свою электронную почту",
                    email: "Ваш адрес электронной почты должен быть в формате name@domain.com"
                },
                message: "Пожалуйста задайте свой вопрос"
            }
        });
    }

    valideForms('#form');
    valideForms('#form-modal');

    //Mask phone in form
    $('input[name=phone]').mask("+7 (999) 999-99-99");

    //Form submission
    $('form').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "mailer/smart.php",
            data: $(this).serialize()
        }).done(function () {
            $(this).find("input").val("");
            $('#order').fadeOut();
            $('.overlay, #thanks').fadeIn('slow');

            $('form').trigger('reset');
            $('form-modal').trigger('reset');
        });
        return false;
    });
    $('.burger').on("click", function (event) {
        event.preventDefault();
        $('nav').toggleClass("nav-active");
        $('.burger').toggleClass("burger-active");
    });

    //Плавное появление в связке с animated.css
    new WOW().init();

});

